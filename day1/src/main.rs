use itertools::Itertools;
use std::fs;
use std::io;
use std::io::prelude::*;
use std::path;

fn main() {
    let path = path::Path::new("input");
    let (first, second) = problems(&path);
    println!("problem 1: {}", first);
    println!("problem 2: {}", second);
}

fn problems(path: &path::Path) -> (i32, i32) {
    let file = fs::File::open(&path).unwrap();
    let reader = io::BufReader::new(file);
    let lines = reader.lines();

    let mut elf_calories = Vec::new();
    for (_key, group) in &lines
        .map(|line| line.unwrap())
        .group_by(|check| *check == String::from(""))
    {
        let numbers: Vec<String> = group.collect();
        assert!(!numbers.is_empty());
        let calories = numbers
            .into_iter()
            .map(|number| number.parse::<i32>().unwrap_or(0))
            .sum::<i32>();
        elf_calories.push(calories);
    }
    let first = *elf_calories.iter().max().unwrap();
    let second = elf_calories
        .iter()
        .sorted_by(|a, b| b.cmp(a))
        .take(3)
        .sum::<i32>();
    (first, second)
}

#[test]
fn instruction() {
    let path = path::Path::new("instruction_input");
    let (first, second) = problems(&path);
    assert_eq!(first, 24000);
    assert_eq!(second, 45000);
}
