use std::fs;
use std::io;
use std::io::prelude::*;
use std::path;

#[derive(Debug, Clone, Copy)]
enum Choices {
    Rock,
    Paper,
    Siccor,
}

#[derive(Debug, Clone, Copy)]
enum Todo {
    Loose,
    Draw,
    Win,
}

fn main() {
    let path = path::Path::new("input");
    println!("problem1: {}", problem1(path));
    println!("problem2: {}", problem2(path));
}

fn problem1(path: &path::Path) -> i32 {
    let file = fs::File::open(&path).unwrap();
    let reader = io::BufReader::new(file);
    let lines = reader.lines();

    let input: Vec<_> = lines
        .map(|line| {
            let line = line.unwrap();
            let mut choices = line.split(" ").map(|word| match word {
                "A" | "X" => Choices::Rock,
                "B" | "Y" => Choices::Paper,
                "C" | "Z" => Choices::Siccor,
                _ => unreachable!(),
            });
            (choices.next().unwrap(), choices.next().unwrap())
        })
        .collect();
    let problem_scores: Vec<_> = input
        .into_iter()
        .map(|(first, second)| {
            //
            let shape_score = match second {
                Choices::Rock => 1,
                Choices::Paper => 2,
                Choices::Siccor => 3,
            };
            let win_score = match (first, second) {
                (Choices::Rock, Choices::Paper) => 6,
                (Choices::Rock, Choices::Siccor) => 0,
                (Choices::Paper, Choices::Rock) => 0,
                (Choices::Paper, Choices::Siccor) => 6,
                (Choices::Siccor, Choices::Rock) => 6,
                (Choices::Siccor, Choices::Paper) => 0,
                _ => 3,
            };
            shape_score + win_score
        })
        .collect();
    problem_scores.into_iter().sum::<i32>()
}

fn problem2(path: &path::Path) -> i32 {
    let file = fs::File::open(&path).unwrap();
    let reader = io::BufReader::new(file);
    let lines = reader.lines();

    let input: Vec<_> = lines
        .map(|line| {
            let line = line.unwrap();
            let mut words = line.split(" ");
            let choice = match words.next().unwrap() {
                "A" => Choices::Rock,
                "B" => Choices::Paper,
                "C" => Choices::Siccor,
                _ => unreachable!(),
            };
            let todo = match words.next().unwrap() {
                "X" => Todo::Loose,
                "Y" => Todo::Draw,
                "Z" => Todo::Win,
                _ => unreachable!(),
            };
            (choice, todo)
        })
        .collect();
    let problem_scores: Vec<_> = input
        .into_iter()
        .map(|(opponent, todo)| {
            let my_hand = match (opponent, todo) {
                (Choices::Rock, Todo::Loose) => Choices::Siccor,
                (Choices::Rock, Todo::Win) => Choices::Paper,

                (Choices::Paper, Todo::Loose) => Choices::Rock,
                (Choices::Paper, Todo::Win) => Choices::Siccor,

                (Choices::Siccor, Todo::Loose) => Choices::Paper,
                (Choices::Siccor, Todo::Win) => Choices::Rock,

                (x, Todo::Draw) => x,
            };
            let shape_score = match my_hand {
                Choices::Rock => 1,
                Choices::Paper => 2,
                Choices::Siccor => 3,
            };
            let win_score = match (opponent, my_hand) {
                (Choices::Rock, Choices::Paper) => 6,
                (Choices::Rock, Choices::Siccor) => 0,
                (Choices::Paper, Choices::Rock) => 0,
                (Choices::Paper, Choices::Siccor) => 6,
                (Choices::Siccor, Choices::Rock) => 6,
                (Choices::Siccor, Choices::Paper) => 0,
                _ => 3,
            };
            shape_score + win_score
        })
        .collect();
    problem_scores.into_iter().sum::<i32>()
}
