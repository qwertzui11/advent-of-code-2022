use std::collections::HashSet;
use std::fs;
use std::io;
use std::io::prelude::*;
use std::path;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let path = path::Path::new("input");
    println!("problem1: {}", problem1(path)?);
    println!("problem2: {}", problem2(path)?);
    Ok(())
}

fn problem2(path: &path::Path) -> Result<u32, Box<dyn std::error::Error>> {
    let file = fs::File::open(&path)?;
    let reader = io::BufReader::new(file);
    let lines: Result<Vec<String>, _> = reader.lines().collect();
    let lines = lines?;
    assert_eq!(lines.len() % 3, 0);
    lines
        .chunks_exact(3)
        .map(|rucksäcke| {
            let priority = find_group_priority(rucksäcke)?;
            Ok(priority_of_item(priority))
        })
        .sum()
}

fn find_group_priority(rucksäcke: &[String]) -> Result<char, Box<dyn std::error::Error>> {
    let first: HashSet<char> = rucksäcke[0].chars().collect();
    let second: HashSet<char> = rucksäcke[1].chars().collect();
    let first_second_common: HashSet<char> = first.intersection(&second).copied().collect();

    let third: HashSet<_> = rucksäcke[2].chars().collect();
    let mut common_items = third.intersection(&first_second_common);

    let result = common_items.next().copied().unwrap();
    assert!(common_items.next().is_none());
    Ok(result)
}

fn problem1(path: &path::Path) -> Result<u32, Box<dyn std::error::Error>> {
    let file = fs::File::open(&path)?;
    let reader = io::BufReader::new(file);
    let lines = reader.lines();

    lines
        .map(|line| {
            let line = line?;
            assert_eq!(line.len() % 2, 0);
            let middle = line.len() / 2;
            let first_compartment = &line[..middle];
            let second_compartment = &line[middle..];
            let common_item = common_item(first_compartment, second_compartment);
            Ok(priority_of_item(common_item))
        })
        .sum()
}

fn priority_of_item(item: char) -> u32 {
    if item.is_ascii_lowercase() {
        return item as u32 - 'a' as u32 + 1;
    }
    if item.is_ascii_uppercase() {
        return item as u32 - 'A' as u32 + 27;
    }
    unreachable!();
}

fn common_item(first: &str, second: &str) -> char {
    let first: HashSet<_> = first.chars().collect();
    let second: HashSet<_> = second.chars().collect();
    let mut common_items = first.intersection(&second);
    let result = common_items.next().unwrap();
    assert!(common_items.next().is_none());
    *result
}
