use std::fs;
use std::io;
use std::io::prelude::*;
use std::num::ParseIntError;
use std::path;
use std::str::FromStr;

fn main() {
    let path = path::Path::new("input");
    let file = fs::File::open(&path).unwrap();
    let reader = io::BufReader::new(file);
    let lines = reader.lines();

    let input: Vec<SectionPair> = lines
        .map(|line| line.unwrap().parse::<SectionPair>().unwrap())
        .collect();
    println!("problem1: {}", problem1(&input));
    println!("problem2: {}", problem2(&input));
}

struct Section {
    from: i32,
    to: i32,
}

impl FromStr for Section {
    type Err = ParseIntError;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let from_to: Result<Vec<i32>, _> = input.split("-").map(|b| b.parse::<i32>()).collect();
        let mut from_to_iter = from_to?.into_iter();
        let (Some(from), Some(to) , None) = (from_to_iter.next(), from_to_iter.next(), from_to_iter.next()) else {
            // a proper error would be better
            panic!("invalid section");
        };
        Ok(Self { from, to })
    }
}

impl Section {
    fn contains(&self, other: &Section) -> bool {
        self.from <= other.from && self.to >= other.to
    }

    fn overlaps(&self, other: &Section) -> bool {
        self.contains(other)
            || self.from.clamp(other.from, other.to) == self.from
            || self.to.clamp(other.from, other.to) == self.to
    }
}

impl FromStr for SectionPair {
    type Err = ParseIntError;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let elf_sections: Result<Vec<_>, _> = input
            .split(",")
            .map(|word| word.parse::<Section>())
            .collect();
        let mut elf_sections_iter = elf_sections?.into_iter();
        let (Some(first), Some(second) , None) = (elf_sections_iter.next(), elf_sections_iter.next(), elf_sections_iter.next()) else {
            // a proper error would be better
            panic!("invalid elf");
        };
        Ok(Self { first, second })
    }
}

struct SectionPair {
    first: Section,
    second: Section,
}

fn problem1(input: &Vec<SectionPair>) -> i32 {
    input
        .iter()
        .filter(|section| {
            section.first.contains(&section.second) || section.second.contains(&section.first)
        })
        .count()
        .try_into()
        .unwrap()
}

fn problem2(input: &Vec<SectionPair>) -> i32 {
    input
        .iter()
        .filter(|section| {
            section.first.overlaps(&section.second) || section.second.overlaps(&section.first)
        })
        .count()
        .try_into()
        .unwrap()
}
