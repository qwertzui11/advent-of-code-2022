use itertools::Itertools;
use std::fs;
use std::io;
use std::io::prelude::*;
use std::path;

fn main() {
    let path = path::Path::new("input");
    let file = fs::File::open(&path).unwrap();
    let reader = io::BufReader::new(file);
    let lines: Vec<String> = reader.lines().map(|a| a.unwrap()).collect();

    // intial stacks
    let mut lines_iter = lines.iter();
    let rows: Vec<Vec<Option<char>>> = lines_iter
        .by_ref()
        .take(8)
        .map(|line| parse_line(line))
        .collect();
    let mut stacks = Vec::<Vec<char>>::new();
    stacks.resize(9, Vec::<char>::new());
    for row in rows.into_iter().rev() {
        for (index, item) in row.into_iter().enumerate() {
            if let Some(item) = item {
                stacks[index].push(item);
            }
        }
    }
    for _skip_empty_lines in 0..2 {
        lines_iter.by_ref().next().unwrap();
    }

    // moves
    let moves: Vec<Move> = lines_iter.by_ref().map(|line| parse_move(line)).collect();

    // results
    println!("problem1: {}", problem1(stacks.clone(), &moves));
    println!("problem2: {}", problem2(stacks.clone(), &moves));
}

fn problem2(mut stacks: Vec<Vec<char>>, moves: &Vec<Move>) -> String {
    for move_ in moves {
        let move_from_stack = &mut stacks[move_.from - 1];
        let move_from_stack_index = move_from_stack.len() - move_.count;
        let to_copy = move_from_stack.split_off(move_from_stack_index);
        stacks[move_.to - 1].extend_from_slice(&to_copy);
    }
    stacks
        .into_iter()
        .map(|stack| *stack.last().unwrap())
        .collect()
}

fn problem1(mut stacks: Vec<Vec<char>>, moves: &Vec<Move>) -> String {
    for move_ in moves {
        for _ in 0..move_.count {
            let from_value = stacks[move_.from - 1].pop().unwrap();
            stacks[move_.to - 1].push(from_value);
        }
    }
    stacks
        .into_iter()
        .map(|stack| *stack.last().unwrap())
        .collect()
}

fn parse_move(line: &str) -> Move {
    let mut words = line.split(' ');
    words.next().unwrap();
    let count = words.next().unwrap().parse::<usize>().unwrap();
    words.next().unwrap();
    let from = words.next().unwrap().parse::<usize>().unwrap();
    words.next().unwrap();
    let to = words.next().unwrap().parse::<usize>().unwrap();
    assert!(words.next().is_none());
    Move { count, from, to }
}

#[derive(Debug)]
struct Move {
    count: usize,
    from: usize,
    to: usize,
}

fn parse_line(line: &str) -> Vec<Option<char>> {
    let mut row = Vec::<Option<char>>::new();
    for column_item in &line.chars().chunks(4) {
        let column_item: String = column_item.collect();
        if column_item.starts_with("   ") {
            row.push(None);
        } else {
            row.push(Some(column_item.chars().nth(1).unwrap()));
        }
    }
    row
}
