use std::fs;
use std::io;
use std::io::prelude::*;
use std::path;

fn main() {
    let path = path::Path::new("input");
    let file = fs::File::open(&path).unwrap();
    let reader = io::BufReader::new(file);
    let lines: Vec<String> = reader.lines().map(|a| a.unwrap()).collect();
    assert_eq!(lines.len(), 1);
    let line = lines.first().unwrap();
    println!("problem1: {}", problem(&line, 4));
    println!("problem2: {}", problem(&line, 14));
}

fn problem(line: &str, marker_len: usize) -> usize {
    let chars: Vec<(usize, char)> = line.char_indices().collect();
    let found: &[(usize, _)] = chars
        .windows(marker_len)
        .find(|a| {
            let mut chars: Vec<char> = a
                .iter()
                .map(|indexed_character| indexed_character.1)
                .collect();
            chars.sort();
            chars.dedup();
            chars.len() == marker_len
        })
        .unwrap();
    assert_eq!(found.len(), marker_len);
    found.first().unwrap().0 + marker_len
}

#[test]
fn test_problem1() {
    assert_eq!(problem("bvwbjplbgvbhsrlpgdmjqwftvncz", 4), 5);
    assert_eq!(problem("nppdvjthqldpwncqszvftbrmjlhg", 4), 6);
    assert_eq!(problem("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 4), 10);
    assert_eq!(problem("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 4), 11);
}

#[test]
fn test_problem2() {
    assert_eq!(problem("mjqjpqmgbljsphdztnvjfqwrcgsmlb", 14), 19);
    assert_eq!(problem("bvwbjplbgvbhsrlpgdmjqwftvncz", 14), 23);
    assert_eq!(problem("nppdvjthqldpwncqszvftbrmjlhg", 14), 23);
    assert_eq!(problem("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 14), 29);
    assert_eq!(problem("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 14), 26);
}
