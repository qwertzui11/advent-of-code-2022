use std::collections::HashMap;
use std::collections::HashSet;
use std::fs;
use std::io;
use std::io::prelude::*;
use std::path;

fn main() {
    let path = path::Path::new("input");
    let file = fs::File::open(&path).unwrap();
    let reader = io::BufReader::new(file);
    let lines: Vec<String> = reader.lines().map(|l| l.unwrap()).collect();

    let mut current_dir: Vec<String> = Vec::new();
    let mut folders: HashMap<String, Folder> = HashMap::new();

    for line in lines {
        let words: Vec<String> = line.split(" ").map(|w| w.to_string()).collect();
        if line.starts_with("$ ") {
            if line.starts_with("$ cd ") {
                let cd_directory = words.last().unwrap();
                match cd_directory.as_str() {
                    "/" => current_dir.clear(),
                    ".." => {
                        current_dir.pop().unwrap();
                        ()
                    }
                    directory => current_dir.push(directory.to_string()),
                };
            }
            if line.starts_with("$ ls ") {
                // do nothing
            }
        } else {
            let dir = current_dir.join("/");
            if !folders.contains_key(&dir) {
                let insert = Folder::default();
                folders.insert(dir.clone(), insert);
            }
            let folder = folders.get_mut(&dir).unwrap();
            assert_eq!(words.len(), 2);
            if line.starts_with("dir ") {
                folder.folders.insert(words.last().unwrap().to_string());
            } else {
                assert_eq!(words.len(), 2);
                let size = words.first().unwrap().parse::<usize>().unwrap();
                let file = words.last().unwrap().to_string();
                folder.files.insert(file, size);
            }
        }
    }
    //dbg!(&folders);
    let problem1 = folders
        .keys()
        .map(|f| folder_size(f, &folders))
        //.map(|f| dbg!(f))
        .filter(|&f| f <= 100000)
        .sum::<usize>();
    println!("problem1: {}", problem1);

    let needed_space = 30000000;
    let disk_size = 70000000;
    let disk_usage = folder_size("", &folders);
    let to_free = needed_space - (disk_size - disk_usage);
    let problem2 = folders
        .keys()
        .map(|f| folder_size(f, &folders))
        .filter(|&s| s >= to_free)
        .min()
        .unwrap();
    println!("problem2: {}", problem2);
}

fn folder_size(current_dir: &str, folders: &HashMap<String, Folder>) -> usize {
    //dbg!(current_dir);
    let current_folder = folders.get(current_dir).unwrap();
    let mut sum: usize = 0;
    for folder in &current_folder.folders {
        let sub_folder_dir = if current_dir.is_empty() {
            folder.to_string()
        } else {
            format!("{}/{}", current_dir, folder)
        };
        let size = folder_size(&sub_folder_dir, folders);
        sum += size;
    }
    sum += current_folder.files.values().sum::<usize>();
    sum
}

#[derive(Default, Debug)]
struct Folder {
    files: HashMap<String, usize>,
    folders: HashSet<String>,
}
