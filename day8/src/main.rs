use std::collections::HashSet;
use std::fs;
use std::io;
use std::io::prelude::*;
use std::path;

use itertools::Itertools;

fn main() {
    let path = path::Path::new("input");
    let numbers = read_input(path);
    let trees = problem1(&numbers);
    if false {
        print_forrest(&numbers, &trees);
    }
    println!("problem1: {}", trees.len());
    println!("problem2: {}", problem2(&numbers));
}

fn read_input(path: &path::Path) -> Vec<Vec<i32>> {
    let file = fs::File::open(&path).unwrap();
    let reader = io::BufReader::new(file);
    let lines: Vec<String> = reader.lines().map(|a| a.unwrap()).collect();

    lines
        .into_iter()
        .map(|l| {
            l.chars()
                .map(|c| c.to_digit(10).unwrap() as i32)
                .collect::<Vec<i32>>()
        })
        .collect()
}

fn problem2(numbers: &Vec<Vec<i32>>) -> usize {
    let mut result = 0;
    let transposed = transpose(numbers.to_vec());
    for (y, row) in numbers.iter().enumerate() {
        for (x, _) in row.iter().enumerate() {
            let score = trees_in_sight(&numbers, &transposed, x, y);
            result = result.max(score);
        }
    }
    result
}

fn trees_in_sight(trees: &[Vec<i32>], trees_transposed: &[Vec<i32>], x: usize, y: usize) -> usize {
    let row = &trees[y];
    let column = &trees_transposed[x];
    let score_row = trees_in_sight_row(row, x);
    let score_column = trees_in_sight_row(&column, y);
    let score = score_row * score_column;
    score
}

fn trees_in_sight_row(row: &[i32], x: usize) -> usize {
    let height = row[x];
    let mut score = 1;
    let to_the_left_or_up: Vec<i32> = row[..x].iter().rev().copied().collect();
    score *= trees_in_sight_directed_row(&to_the_left_or_up, height);
    let to_the_right_or_down: Vec<i32> = row[x..].iter().skip(1).copied().collect();
    score *= trees_in_sight_directed_row(&to_the_right_or_down, height);
    score
}

fn trees_in_sight_directed_row(row: &[i32], height: i32) -> usize {
    let furthest_distance = match row.iter().find_position(|&&check| check >= height) {
        Some(found) => found.0 + 1,
        None => row.len(),
    };
    furthest_distance
}

fn print_forrest(trees: &[Vec<i32>], highlights: &HashSet<(usize, usize)>) {
    for (y, row) in trees.iter().enumerate() {
        for (x, number) in row.iter().enumerate() {
            let marked = highlights.get(&(x, y)).is_some();
            if marked {
                print!("\x1b[93m");
            }
            print!("{}", number);
            if marked {
                print!("\x1b[0m");
            }
        }
        println!();
    }
}

fn problem1(numbers: &Vec<Vec<i32>>) -> HashSet<(usize, usize)> {
    let mut found = HashSet::<(usize, usize)>::new();
    for (y, row) in numbers.iter().enumerate() {
        for (x, _) in row.iter().enumerate() {
            if is_visible_at_edge(row, x) {
                found.insert((x, y));
            }
        }
    }
    let numbers = transpose(numbers.to_vec());
    for (x, column) in numbers.iter().enumerate() {
        for (y, _) in column.iter().enumerate() {
            if is_visible_at_edge(column, y) {
                found.insert((x, y));
            }
        }
    }
    found
}

fn is_visible_at_edge(row: &[i32], x: usize) -> bool {
    let height: i32 = *row.get(x).unwrap();
    let max_right = *row[(x + 1)..].iter().max().unwrap_or(&-1);
    let max_left = *row[..x].iter().max().unwrap_or(&-1);
    max_left < height || max_right < height
}

// https://stackoverflow.com/questions/64498617/how-to-transpose-a-vector-of-vectors-in-rust
fn transpose<T>(v: Vec<Vec<T>>) -> Vec<Vec<T>> {
    assert!(!v.is_empty());
    let len = v[0].len();
    let mut iters: Vec<_> = v.into_iter().map(|n| n.into_iter()).collect();
    (0..len)
        .map(|_| {
            iters
                .iter_mut()
                .map(|n| n.next().unwrap())
                .collect::<Vec<T>>()
        })
        .collect()
}

#[test]
fn test_problem2_trees_in_sight_2_1() {
    let path = path::Path::new("test_input");
    let numbers = read_input(path);
    let transposed = transpose(numbers.to_vec());
    assert_eq!(trees_in_sight(&numbers, &transposed, 2, 1), 4);
}

#[test]
fn test_problem2_trees_in_sight_2_3() {
    let path = path::Path::new("test_input");
    let numbers = read_input(path);
    let transposed = transpose(numbers.to_vec());
    assert_eq!(trees_in_sight(&numbers, &transposed, 2, 3), 8);
}

#[test]
fn test_problem2() {
    let path = path::Path::new("test_input");
    let numbers = read_input(path);
    assert_eq!(problem2(&numbers), 8);
}
