use std::collections::HashSet;
use std::fs;
use std::io;
use std::io::prelude::*;
use std::path;
use std::str::FromStr;

fn main() {
    let input = read_input(path::Path::new("input"));
    println!("problem1: {}", problem1(&input));
    println!("problem2: {}", problem2(&input));
}

fn read_input(path: &path::Path) -> Vec<(Direction, i32)> {
    let file = fs::File::open(&path).unwrap();
    let reader = io::BufReader::new(file);
    let lines: Vec<String> = reader.lines().map(|a| a.unwrap()).collect();

    lines
        .into_iter()
        .map(|l| {
            let mut words = l.split(" ");
            let direction = words.next().unwrap().parse().unwrap();
            let count = words.next().unwrap().parse().unwrap();
            assert!(words.next().is_none());
            (direction, count)
        })
        .collect()
}

fn problem2(input: &Vec<(Direction, i32)>) -> usize {
    let mut visited: HashSet<(i32, i32)> = HashSet::new();
    let mut head: (i32, i32) = (0, 0);
    let mut tails: Vec<(i32, i32)> = Vec::new();
    tails.resize(9, (0, 0));
    for move_ in input {
        assert_eq!(tails.len(), 9);
        let (move_head_x, move_head_y) = match move_.0 {
            Direction::Left => (-move_.1, 0),
            Direction::Right => (move_.1, 0),
            Direction::Up => (0, move_.1),
            Direction::Down => (0, -move_.1),
        };
        for _ in 0..move_head_x.abs() {
            head.0 += move_head_x.signum();
            tails = calculate_tail(&head, &tails);
            visited.insert(*tails.last().unwrap());
        }
        for _ in 0..move_head_y.abs() {
            head.1 += move_head_y.signum();
            tails = calculate_tail(&head, &tails);
            visited.insert(*tails.last().unwrap());
        }
    }
    if false {
        print_rope(&visited);
    }
    visited.len()
}

fn print_rope(visited: &HashSet<(i32, i32)>) {
    let min = visited.iter().fold((0, 0), |result, check| {
        (result.0.min(check.0), result.1.min(check.1))
    });
    let max = visited.iter().fold((0, 0), |result, check| {
        (result.0.max(check.0), result.1.max(check.1))
    });
    for y in min.1..=max.1 {
        for x in min.0..=max.0 {
            if visited.contains(&(x, y)) {
                print!("#");
            } else {
                print!(".");
            }
        }
        println!();
    }
}

fn calculate_tail(head: &(i32, i32), tails: &[(i32, i32)]) -> Vec<(i32, i32)> {
    let tails: Vec<(i32, i32)> = tails
        .into_iter()
        .fold(vec![head.clone()], |mut result, tail| {
            let head = result.last().unwrap();
            let (move_tail_x, move_tail_y) = move_tail(&head, &tail);
            let mut tail = tail.clone();
            tail.0 += move_tail_x;
            tail.1 += move_tail_y;
            result.push(tail);
            result
        });
    tails.into_iter().skip(1).collect()
}

fn problem1(input: &Vec<(Direction, i32)>) -> usize {
    let mut visited: HashSet<(i32, i32)> = HashSet::new();
    let mut head: (i32, i32) = (0, 0);
    let mut tail: (i32, i32) = (0, 0);
    for move_ in input {
        let (move_head_x, move_head_y) = match move_.0 {
            Direction::Left => (-move_.1, 0),
            Direction::Right => (move_.1, 0),
            Direction::Up => (0, move_.1),
            Direction::Down => (0, -move_.1),
        };
        for _ in 0..move_head_x.abs() {
            head.0 += move_head_x.signum();
            let (move_tail_x, move_tail_y) = move_tail(&head, &tail);
            tail.0 += move_tail_x;
            tail.1 += move_tail_y;
            visited.insert(tail);
        }
        for _ in 0..move_head_y.abs() {
            head.1 += move_head_y.signum();
            let (move_tail_x, move_tail_y) = move_tail(&head, &tail);
            tail.0 += move_tail_x;
            tail.1 += move_tail_y;
            visited.insert(tail);
        }
    }
    visited.len()
}

fn move_tail(head: &(i32, i32), tail: &(i32, i32)) -> (i32, i32) {
    let diff_x = head.0 - tail.0;
    let diff_y = head.1 - tail.1;
    if diff_x.abs() <= 1 && diff_y.abs() <= 1 {
        return (0, 0);
    }
    assert_eq!(diff_x.clamp(-2, 2), diff_x);
    assert_eq!(diff_y.clamp(-2, 2), diff_y);
    (diff_x.signum(), diff_y.signum())
}

enum Direction {
    Up,
    Left,
    Right,
    Down,
}

impl FromStr for Direction {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "U" => Direction::Up,
            "D" => Direction::Down,
            "R" => Direction::Right,
            "L" => Direction::Left,
            _ => unreachable!(),
        })
    }
}

#[test]
fn test_move_tail() {
    assert_eq!(move_tail(&(1, 0), &(0, 0)), (0, 0));
    assert_eq!(move_tail(&(1, 1), &(0, 0)), (0, 0));
    assert_eq!(move_tail(&(0, 2), &(0, 0)), (0, 1));
    assert_eq!(move_tail(&(2, 0), &(0, 0)), (1, 0));
}

#[test]
fn test_problem1() {
    let input = read_input(path::Path::new("test_input"));
    assert_eq!(problem1(&input), 13);
}

#[test]
fn test_problem2() {
    let input = read_input(path::Path::new("test_input_problem2"));
    assert_eq!(problem2(&input), 36);
}
