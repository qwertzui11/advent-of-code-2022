.PHONY: build run test

build:
	cargo check

run:
	cargo run

test:
	cargo test

